use std::{thread, time};
use std::collections::HashMap;
use std::fs;
use std::process::Command;

use assert_cmd::prelude::*;

// wait for application to launch
static SLEEP_DURATION: time::Duration = time::Duration::from_secs(3);

#[test]
fn interactive_defaults() -> Result<(), Box<dyn std::error::Error>> {
    test_server(&[], 8080, "", 200, None, None)
}

#[test]
fn interactive_custom_port() -> Result<(), Box<dyn std::error::Error>> {
    test_server(&["-p", "8081"], 8081, "", 200, None, None)
}

#[test]
fn interactive_custom_status() -> Result<(), Box<dyn std::error::Error>> {
    // custom port since test run in parallel
    test_server(&["-p", "8082", "-s", "202"], 8082, "", 202, None, None)
}

#[test]
fn interactive_defaults_with_headers() -> Result<(), Box<dyn std::error::Error>> {
    // headers are a little weird because minreq stores them this way. Makes for easier assertions.
    let mut headers = HashMap::new();
    headers.insert("contenttype".to_string(), " application/json".to_string());
    headers.insert("content-encoding".to_string(), " UTF-8".to_string());

    // custom port since test run in parallel
    test_server(
        &[
            "-p",
            "8083",
            "-h",
            "ContentType: application/json",
            "--header",
            "Content-Encoding: UTF-8",
        ],
        8083,
        "",
        200,
        Some(headers),
        None,
    )
}

#[test]
fn interactive_defaults_with_path_json_payload() -> Result<(), Box<dyn std::error::Error>> {
    let payload = fs::read("tests/resources/sample_payload.json")?;
    test_server(
        &["-p", "8084", "-d", "@tests/resources/sample_payload.json"],
        8084,
        "",
        200,
        None,
        Some(payload),
    )
}

#[test]
fn interactive_defaults_with_custom_json_payload() -> Result<(), Box<dyn std::error::Error>> {
    let payload = fs::read_to_string("tests/resources/sample_payload.json")?;
    test_server(
        &["-p", "8085", "-d", &payload.clone()],
        8085,
        "",
        200,
        None,
        Some(payload.into_bytes()),
    )
}

#[test]
fn interactive_defaults_with_binary_payload() -> Result<(), Box<dyn std::error::Error>> {
    let payload = fs::read("tests/resources/doge.jpg")?;
    test_server(
        &["-p", "8086", "-d", "@tests/resources/doge.jpg"],
        8086,
        "",
        200,
        None,
        Some(payload),
    )
}

#[test]
fn interactive_invalid_port() -> Result<(), Box<dyn std::error::Error>> {
    Command::new("target/debug/servers")
        .args(&["-p", "70000"])
        .assert()
        .failure()
        .code(2);

    Ok(())
}

#[test]
fn interactive_invalid_status_code() -> Result<(), Box<dyn std::error::Error>> {
    Command::new("target/debug/servers")
        .args(&["-s", "600"])
        .assert()
        .failure()
        .code(2);

    Ok(())
}

#[test]
fn config_file_not_found() -> Result<(), Box<dyn std::error::Error>> {
    Command::new("target/debug/servers")
        .arg("-c")
        .arg("/file/not/found")
        .assert()
        .failure()
        .code(1);

    Ok(())
}

#[test]
fn config_file() -> Result<(), Box<dyn std::error::Error>> {
    // headers are a little weird because minreq stores them this way. Makes for easier assertions.
    let mut headers = HashMap::new();
    headers.insert("contenttype".to_string(), " application/json".to_string());

    test_server(
        &["-c", "tests/resources/servers.yml"],
        8087,
        "/test",
        200,
        Some(headers),
        Some("{\"foo\": true, \"bar\": \"baz\"}".to_string().into_bytes()),
    )
}

fn test_server(
    args: &[&str],
    port: usize,
    path: &str,
    status_code: i32,
    response_headers: Option<HashMap<String, String>>,
    response_data: Option<Vec<u8>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut server = Command::new("target/debug/servers").args(args).spawn()?;
    thread::sleep(SLEEP_DURATION);

    let response = minreq::get("http://127.0.0.1:".to_owned() + &port.to_string() + &path.to_string()).send()?;
    server.kill()?;

    // asserts at end to ensure server is being killed
    assert_eq!(response.status_code, status_code);

    if let Some(headers) = response_headers {
        headers
            .iter()
            .for_each(|(key, value)| assert_eq!(response.headers.get(key), Some(value)));
    }

    if let Some(data) = response_data {
        assert_eq!(data, response.into_bytes());
    }

    Ok(())
}
