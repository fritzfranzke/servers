# ChangeLog

## [0.4.1](https://gitlab.com/fritzfranzke/servers/tags/0.4.1) (2021-10-22)

**Added**
* Ability to specify body in configured mode

## [0.4.0](https://gitlab.com/fritzfranzke/servers/tags/0.4.0) (2021-10-22)

**Changed**
* CLI arguments
* Switch to [log4rs](https://docs.rs/log4rs/)

**Added**
* Ability to specify headers and body as CLI args
* Show banner on startup
* More tests

## [0.3.0](https://gitlab.com/fritzfranzke/servers/tags/0.3.0) (2021-03-09)

**Added**
* Possibility to define response headers
* Logging of request body
* Pretty print JSON bodies
* Basic testing

**Changed**
* Use Tide's built-in logger


## [0.2.0](https://gitlab.com/fritzfranzke/servers/tags/0.2.0) (2020-08-16)

**Added:**
* Possibility to define ports in `configured` mode


## [0.1.0](https://gitlab.com/fritzfranzke/servers/tags/0.1.0) (2020-08-16)
Initial release
