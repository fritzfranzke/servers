# ServeRS

## Table of contents
1. [Introduction](#introduction)
2. [Build](#build)
3. [Run](#run)
4. [Configuration](#run)
    1. [Parameters](#parameters)
    2. [Logging](#logging)


## Introduction

Application that allows spawning custom HTTP servers. 
Incoming requests are logged for debugging purposes.
The server is written in [Rust](https://www.rust-lang.org/). 
If serving files is your (sole) use case this may not be the correct application for you.
Use something like [miniserve](https://github.com/svenstaro/miniserve) instead.


## Build 

1. Install the [Rust compiler](https://www.rust-lang.org/tools/install)
2. Compile the application. By default, the binary can be found in the `target/release` directory. 
   
        cargo build --release


## Run

ServeRS can be configured in two ways, either by passing arguments directly or by providing a [YAML](https://yaml.org/) config file.
Note that you can either provide port/status-code/header/body fields, or a config file, not both.
In its simplest form ServeRS can be run using the following command. 
It will spawn a server listening to all requests on port `8080` and return status code `200` for all requests: 

    ./servers

Specify port and status code:

    ./servers -p 8081 -s 202

Specify header and body:

    ./servers -H 'Content-Type: application/json' -d '{"foo": true, "bar": "baz"}'

Specify body from file:

    ./servers -d @/path/to/file
   

## Configuration

### Parameters

In general the parameters are similar to [curl](https://curl.se/). 
For reference see the table below:

| Parameter                      | Description                                                                              | Default       |
| :----------------------------- | :--------------------------------------------------------------------------------------- | :------------ |
| Port: `-p`, `--port`           | Sets the server's ports. Conflicts with `-c`.                                            | `8080`        |
| Status Code: `-s`, `--status`  | Sets the HTTP status code to return.  Conflicts with `-c`.                               | `200`         |
| Header: `-h`, `--header`       | Sets HTTP headers to include in response.  Conflicts with `-c`.                          | -             |
| Body: `-d`, `--data`           | Sets body to include in response. Prepend `@` to specify path.                           | -             |
| Config File: `-c`, `--config`  | Sets the YAML file containing server specification. Conflicts with both `-p` and `-s`.   | `servers.yml` |

An example for a config file can be found in the `endpoints.yml` file.


### Logging
The application uses the crate [log4rs](https://docs.rs/log4rs/) for logging. 
As such, logging can be configured by editing the `resources/log4rs.yml` file.
By default, the application logs requests to `stdout` and the `logs/servers.log` file.
Check the official docs for details on customizing the logging behaviour.
